package com.imperial.rebels.endpoints;

import com.imperial.rebels.dto.Rebel;
import com.imperial.rebels.services.RebelsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/rebels")
public class MainController {

    @Autowired
    RebelsService rebelsService;

    @PostMapping(value="/register",produces= MediaType.APPLICATION_JSON_VALUE)
    public boolean registerRebelsWithoutConcurrency(@RequestBody @NotNull @NotEmpty List<Rebel> rebelsList){
        return rebelsService.registerRebels(rebelsList);
    }

    @PostMapping(value="/register/concurrently",produces= MediaType.APPLICATION_JSON_VALUE)
    public boolean registerRebelsConcurrently(@RequestBody @NotNull @NotEmpty List<Rebel> rebelsList){
        return rebelsService.registerRebelsConcurrently(rebelsList);
    }

}
