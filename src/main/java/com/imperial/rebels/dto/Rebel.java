package com.imperial.rebels.dto;

import lombok.Data;

@Data
public class Rebel {
    String name;
    String planet;
}
