package com.imperial.rebels.services;

import com.imperial.rebels.dto.Rebel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class RebelsService {
    private static Logger log = LoggerFactory.getLogger(RebelsService.class);

    @Value("${spring.rebels.text.path}")
    private String PATH_TO_SAVE;

    @Value("${spring.rebels.date.format}")
    private String DATE_FORMAT;

    //To maintain the concurrency and atomicity when used from different threads
    AtomicInteger atomicInt;


    public boolean registerRebels(List<Rebel> rebelsList) {
        atomicInt = new AtomicInteger(0);

        //Multithreading interactions
        rebelsList.parallelStream().forEach(rebel -> {

            if (validateDto(rebel))
                writeToFile(rebel);
        });


        return isAllSaved(rebelsList);
    }

    public boolean registerRebelsConcurrently(List<Rebel> rebelsList) {
        atomicInt = new AtomicInteger(0);

        //Traditional loop is faster (compared with lambdas,foreach(), map() ..)
        for (Rebel rebel : rebelsList) {
            if (validateDto(rebel))
                writeToFile(rebel);
        }

        return isAllSaved(rebelsList);
    }

    private boolean isAllSaved(List<Rebel> list) {
        boolean result = list != null && !list.isEmpty() && atomicInt.get() == list.size();

        if (!result)
            log.warn("RebelsService | isAllSaved() : Data not completely saved, the list size is {}, and only {} elements were saved", list.size(), atomicInt.get());
        else
            log.info("RebelsService | isAllSaved() : Data completely saved, the list size is {}, and {} elements were saved", list.size(), atomicInt.get());

        return result;
    }

    private void writeToFile(Rebel rebel) {
        try {
            Files.write(Paths.get(PATH_TO_SAVE), (buildTextToSave(rebel) + System.lineSeparator()).getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            atomicInt.incrementAndGet();
        } catch (IOException e) {
            log.error("RebelsService | writeToFile() : ERROR SAVING !! " + e.getLocalizedMessage());
        }
    }

    private String buildTextToSave(Rebel rebel) {
        String result = "";
        try {
            result = "Rebel " + rebel.getName() + " on " + rebel.getPlanet() + " at " + getDate();
        } catch (NullPointerException e) {
            log.error("RebelsService | buildTextToSave() : " + e.getLocalizedMessage());
        }
        return result;
    }

    private boolean validateDto(Rebel rebel) {
        return Objects.nonNull(rebel) && rebel.getName() != null && rebel.getPlanet() != null;
    }

    private String getDate() {
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
        return formatter.format(new Date());
    }
}
