package com.imperial.rebels;

import com.imperial.rebels.dto.Rebel;
import com.imperial.rebels.services.RebelsService;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RebelsServiceTests {

    final static String NAME = "TEST_NAME";
    final static String PLANET = "TEST_PLANET";

    @Value("${spring.rebels.text.path}")
    private String PATH_TO_SAVE;

    @Autowired
    RebelsService rebelsService;

    List<Rebel> rebelsList = new ArrayList<>();

    @Before
    public void init(){
        mockRebelsList(1000);
    }

    private void mockRebelsList(int size){
        rebelsList = new ArrayList<>();
        for(int i=0; i < size; i++){
            rebelsList.add(mockRebel(i));
        }
    }

    private Rebel mockRebel(int index){
        Rebel rebel = new Rebel();
        rebel.setName(NAME + index);
        rebel.setPlanet(PLANET + index);
        return rebel;
    }

    private Rebel mockErroneousRebel(){
        Rebel rebel = new Rebel();
        rebel.setName(null);
        rebel.setPlanet(null);
        return rebel;
    }

    private void addErrorToList(){
        rebelsList.add(mockErroneousRebel());
    }

    @Test
    public void registerRebelsConcurrentlyTest() {
        mockRebelsList(1000);
        boolean result = rebelsService.registerRebelsConcurrently(rebelsList);
        Assert.assertTrue(result);
    }

    @Test
    public void readFileTest() throws IOException {
        mockRebelsList(1000);

        File f = new File(PATH_TO_SAVE);
        f.delete();

        rebelsService.registerRebelsConcurrently(rebelsList);

        List<String> allLines = Files.readAllLines(Paths.get(PATH_TO_SAVE));

        Assert.assertTrue(allLines.size() == 1000);
    }

    @Test
    public void registerRebelsConcurrentlyErroneousTest() {
        addErrorToList();
        boolean result = rebelsService.registerRebelsConcurrently(rebelsList);
        Assert.assertFalse(result);
    }

    @Test
    public void registerRebels() {
        mockRebelsList(1000);
        boolean result = rebelsService.registerRebels(rebelsList);
        Assert.assertTrue(result);
    }

    @Test
    public void registerRebelsErroneousTest() {
        addErrorToList();
        boolean result = rebelsService.registerRebels(rebelsList);
        Assert.assertFalse(result);
    }

}
