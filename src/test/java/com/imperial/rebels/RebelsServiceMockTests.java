package com.imperial.rebels;

import com.imperial.rebels.services.RebelsService;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RebelsServiceMockTests {

    RebelsService rebelsService;


    @Before
    public void init(){
        Mockito.when(rebelsService.registerRebels(any())).thenReturn(true);
        Mockito.when(rebelsService.registerRebelsConcurrently(any())).thenReturn(true);
    }


}
